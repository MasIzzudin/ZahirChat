import { AppRegistry } from 'react-native';
import App from './src';
import firebase from 'firebase';

var config = {
  apiKey: "AIzaSyDSlDVJW_aKQ7q40mHCHuEpJjTgukiYJv4",
  authDomain: "zahirchat-f5a9b.firebaseapp.com",
  databaseURL: "https://zahirchat-f5a9b.firebaseio.com",
  projectId: "zahirchat-f5a9b",
  storageBucket: "zahirchat-f5a9b.appspot.com",
  messagingSenderId: "858901366230"
};
firebase.initializeApp(config);
  
AppRegistry.registerComponent('ZahirChat', () => App);
